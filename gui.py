from tkinter import *
from tkinter import ttk, Menu, scrolledtext
import requests
import json

def make_transaction():
	# an theloume sto idio tab apla allagi window2 <->tab2 
	# !!!
	window2 = Tk()
	window2.title("New Transaction")
	txt1 = scrolledtext.ScrolledText(window2)
	txt1.grid(row=5)

	amount = spinvar.get()
	password1 = entryvar1_2.get()
	recipient = entryvar1_1.get()

	#print("amount is : " + str(amount))
	#print("password is : " + str(password1))
	#print("recipient : " + str(recipient))
	
	data = {}
	data['password'] = password1 
	data['recipient'] = recipient
	data['amount'] = amount

	ip = "127.0.0.1"
	port = 5000
	if selected.get()==1:
		#print("1")
		url = 'http://'+ip+':'+str(port)+'/create_transaction/recipient_address'
	elif selected.get()==2:
		#print("2")
		url = 'http://'+ip+':'+str(port)+'/create_transaction/id'

	response = requests.post(url, data=data)
	response1 = response.json()['message']

	txt1.insert(INSERT,'Transaction Response Text Goes Here')
	#txt.insert(INSERT, response1)

def view():
	window3 = Tk()
	window3.title("Transactions in Last Validated Block")
	txt2 = scrolledtext.ScrolledText(window3)
	txt2.grid(row=5)

	password2 = entryvar2.get()
	data = password2
	ip = "127.0.0.1"
	port = 5000
	url = 'http://'+ip+':'+str(port)+'/view'

	response = requests.post(url, data=data)
	response2 = response.json()['message']

	txt.insert(INSERT,'View Transactions Text Goes Here')
	#txt.insert(INSERT, response2)
	print(password2)

def balance():
	password3 = entryvar3.get()

	data = password3
	ip = "127.0.0.1"
	port = 5000
	url = 'http://'+ip+':'+str(port)+'/balance'

	response = requests.post(url, data=data)
	response3 = response.json()['message']

	bal = Label(tab3, text = 'Balance Goes Here')
	#bal = Label(tab3, text = response3)
	bal.grid(row=2, column = 1, sticky = "E")
	print(password3)

#def clicked():
	#select button for recipient address (1)/ id(2) selection
#	print(selected.get())


#Create Window and initialise Tab Layout
window = Tk()
window.title("Noobcash Blockchain Client")
tab_control = ttk.Notebook(window)

#Make Transaction - 1st tab
tab1 = ttk.Frame(tab_control)
tab_control.add(tab1, text='Make Transaction') 

#choose between id and address
selected = IntVar()
rad1 = Radiobutton(tab1,text='recipient_address', value=1, variable = selected)
rad1.grid(column=0, row=0)
rad2 = Radiobutton(tab1,text='recipient_id', value=2, variable = selected)
rad2.grid(column=1, row=0)

labels = []
#Labels for Entry Cells
labels.append(Label(tab1, text= 'recipient'))
labels.append(Label(tab1, text= 'password'))
labels.append(Label(tab1, text= 'amount'))
labels[0].grid(row=1, sticky = "W")
labels[1].grid(row=2, sticky = "W")
labels[2].grid(row=3, sticky = "W")

#Use 2 entry cells for typing and 1 Spin for NBCs amount
#entry 1 - recipient
#entry 2 - password
#entry 3 - amount
entryvar1_1 = StringVar()
entryvar1_2 = StringVar()
entry1_1 = Entry(tab1, textvariable = entryvar1_1)
entry1_2 = Entry(tab1, textvariable = entryvar1_2)
entry1_1.grid(row=1, column=1, sticky="E")
entry1_2.grid(row=2, column=1, sticky="E")

spinvar = IntVar()
spin = Spinbox(tab1, from_=0, to=1000, width = 10, textvariable=spinvar, justify = RIGHT)
spin.grid(row=3, column=1, sticky="E")


#Make Transaction with input values given above
button1 = Button(tab1, text = 'Make Transaction', fg='green', command = make_transaction)
button1.grid(row=4, column=1, sticky="E")

#############################################
#############################################

#View Transactions - 2nd tab
tab2 = ttk.Frame(tab_control)
tab_control.add(tab2, text='View Block')

#Label 
labels.append(Label(tab2, text= 'password'))
labels[len(labels)-1].grid(row=0, sticky="W") 
#Entry
entryvar2 = StringVar()
entry2 = Entry(tab2, textvariable = entryvar2)
entry2.grid(row=0, column=1, sticky="E")

button2 = Button(tab2, text = 'View Transactions', fg='green', command = view)
button2.grid(row=1, column=1, sticky="E")


#############################################
#############################################

#View Wallet Balance - 3rd tab
tab3 = ttk.Frame(tab_control)
tab_control.add(tab3, text='Balance')

#Label 
labels.append(Label(tab3, text= 'password'))
labels[len(labels)-1].grid(row=0, sticky="W") 
#Entry
entryvar3 = StringVar()
entry3 = Entry(tab3,textvariable = entryvar3)
entry3.grid(row=0, column=1, sticky="E")

button3 = Button(tab3, text = 'View Balance', fg='green', command = balance)
button3.grid(row=1, column=1, sticky="E")


# !!!
#neo koubi new transaction poy energopoiei pali ta kelia kai kanei neo transaction

#kodikas pou apenergopoiei to entry keli
# txt = Entry(window,width=10, state='disabled')

#gia na midenisei to spinbox
# spin = selection_clear()

tab_control.pack(expand=1, fill='both')
window.mainloop()
