import requests
import threading
from argparse import ArgumentParser
import json
import common

def send_txs(i, filename, url, cycles):
    f = open(filename, 'r')
    f_c = 0
    while f_c < cycles:
        line = f.readline()
        print("Node: %d: creates transaction %s" % (i, line))
        # το id δίνεται σε μορφή id1 άρα είναι το 3ο στοιχείο της πρώτης "λέξης""
        # το amount είναι η δεύτερη "λέξη"
        recipient_id = line.split()[0][2]
        amount = line.split()[1]
        data = {}
        data['password'] = common.password
        data['recipient_id'] = recipient_id
        data['amount'] = amount
        dic = {'data': json.dumps(data)}
        print('From: %d   To: %s   Amount: %s' % (i, recipient_id, amount))
        response = requests.post(url, data=dic)
        print(response.json()['message'])
        f_c += 1
    return


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-n: ', '--No', default=5,
                        type=int, help='Number of nodes to use')
    parser.add_argument('-c', '--Cy', default=100,
                        type=int, help='Number of transactions for every node')
    args = parser.parse_args()
    nodes = args.No
    cycles = args.Cy

    # η files είναι λίστα με τους file descriptors για τα αρχεία transactions.txt
    # η ports είναι λίστα με τα αντίστοιχα ports για κάθε κόμβο
    ports = [10000, 20000]
    files = []
    ip_address = ['192.168.0.2', '192.168.0.3', '192.168.0.4', '192.168.0.5', '192.168.0.6']
    for i in range(0, nodes):
        if nodes == 5:
            filename = '5nodes/transactions'+str(i)+'.txt'
        elif nodes == 10:
            filename = '10nodes/transactions'+str(i)+'.txt'
        files.append(filename)

    thread_ls = []

    for i in range(0, nodes):
        if nodes == 5:
            url = 'http://'+ip_address[i]+':'+str(ports[0])+'/create_transaction/id'
        else:
            url = 'http://'+ip_address[int(i / 2)]+':'+str(ports[i % 2])+'/create_transaction/id' 
        t = threading.Thread(target = send_txs, args = (i, files[i], url, cycles,))
        t.start()
        thread_ls.append(t)

    for t in thread_ls:
        t.join()
