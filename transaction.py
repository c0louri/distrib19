from collections import OrderedDict
from binascii import hexlify
import json
from datetime import datetime
from Crypto.Hash import SHA
from Crypto.Signature import PKCS1_v1_5

# # # # # # # # # # # # # # # # # #
# λιστα των τιμων του TRANSACTION #
# # # # # # # # # # # # # # # # # #
'''
sender_address -> str
receiver_address -> str
amount -> int
transaction_id -> int
transaction_inputs -> λιστα με τα ids των transaction_outputs
transaction_outputs -> λιστα με τα dicts των transaction_outputs
signature -> str
'''


class Transaction(object):

    def __init__(self, sender_wallet, receiver_address, amount, node):
        # self.sender_address: To public key του wallet από το οποίο προέρχονται τα χρήματα
        # self.receiver_address: To public key του wallet στο οποίο θα καταλήξουν τα χρήματα
        self.node = node
        self.ring = self.node.ring
        self.receiver_address = receiver_address
        receiver_exists = False
        for node in self.ring:
            if node['public_key'] == self.receiver_address:
                receiver_exists = True
                break
        if not receiver_exists:
            raise ValueError('I gotcha lamogio... there is no such receiver!!!')
        # self.amount: το ποσό που θα μεταφερθεί
        self.amount = amount
        # self.transaction_id: το hash του transaction
        # στην περίπτωση του genesis Transaction
        if sender_wallet is None:
            self.sender_address = '0'
            data = str(self.to_dict(for_hash=True)) + str(datetime.now())
            self.transaction_id = str(hash(data))
            self.transaction_inputs = []
            self.transaction_outputs = []
            self.transaction_outputs.append(Transaction_Output(self.transaction_id, receiver_address, amount).to_dict())
            self.signature = "Genesis Transaction you dump!!!"

        # σε διαφορετική περίπτωση
        else:
            self.sender_address = sender_wallet.public_key
            data = str(self.to_dict(for_hash=True)) + str(datetime.now())
            self.transaction_id = str(hash(data))
            # επιλεξε ποια απο τα utxos θα χρησιμοποιηθουν
            # transaction_inputs: λιστα απο τα ids των Transaction_Output
            id = self.node.get_id(sender_wallet.public_key)
            self.transaction_inputs, self.new_amount = self.choose_from_utxos(self.ring[id]['utxos'])
            # αν δεν υπαρχουν αρκετα utxos εμφανισε μηνυμα
            if self.transaction_inputs == []:
                raise ValueError('Not enough utxos you poor!!!')

            # self.transaction_outputs: λίστα από dict Transaction Output
            self.transaction_outputs = self.create_outputs(self.new_amount)
            self.signature = self.sign_transaction(sender_wallet.private_key)

        self.update_utxos()

    # update_utxos from create_transaction
    def update_utxos(self):
        # βρισκουμε τον receiver και τον ενημερωνουμε
        receiver_id = self.node.get_id(self.receiver_address)
        rec_node = self.node.ring[receiver_id]
        key = self.transaction_outputs[0]['id']
        rec_node['utxos'].update({key: self.transaction_outputs[0]})
        # αν ειναι το genesis Transaction δεν ενημερωνεται ο sender
        if self.sender_address == '0':
            return True
        # διαφορετικα βρισκουμε τον sender και τον ενημερωνουμε
        else:
            sender_id = self.node.get_id(self.sender_address)
            sen_node = self.node.ring[sender_id]
            if self.transaction_inputs != []:
                # διαγραφη των utxos που χρησιμοποιηθηκαν
                for utxo in self.transaction_inputs:
                    if sen_node['utxos'].pop(utxo, None) is None:
                        return False
                # αν υπαρχει περισσευμα στο τελευταιο utxo
                if (self.new_amount != 0):
                    # το πεδιο επειδη αυτο ειναι το Transaction_Output προς τον εαυτο του
                    key = self.transaction_outputs[1]['id']
                    sen_node['utxos'].update({key: self.transaction_outputs[1]})
        return True

    # επιστρεφει τα ids των utxo που θα χρησιμοποιηθουν
    # και το amount του καινουργιου utxo αν αυτο περισσευει
    def choose_from_utxos(self, utxos):
        for node in self.ring:
            if (node['public_key'] == self.sender_address):
                running_amount = self.amount
                # λιστα με τα ids των utxos που θα χρησιμοποιηθουν
                used_utxos = []
                for transaction in node['utxos'].values():
                    # αυξησε το πληθος των utxos που θα χρησιμοποιηθουν
                    used_utxos.append(transaction['id'])
                    running_amount -= transaction['amount']
                    # αν τα μεχρι τωρα utxo ειναι αρκετα return
                    if (running_amount <= 0):
                        return used_utxos, -running_amount
                    # αλλιως επανελαβε
                # αν προστεθηκαν ολα, αλλα δεν ειναι αρκετα γυρνα αδεια τη λιστα
                if (running_amount > 0):
                    return [], 0

    def create_outputs(self, new_amount):
        list_of_outputs = []
        # το self.amount ειναι το amount της συναλλαγης που θα μπει στον receiver
        list_of_outputs.append(Transaction_Output(self.transaction_id, self.receiver_address, self.amount).to_dict())
        # το new_amount ειναι το περισσευον amount που θα μπει στον sender
        if new_amount != 0:
            list_of_outputs.append(Transaction_Output(self.transaction_id, self.sender_address, new_amount).to_dict())
        return list_of_outputs

    def sign_transaction(self, private_key):
        # Sign transaction with private key
        signer = PKCS1_v1_5.new(private_key)
        d = OrderedDict()
        d['sender_address'] = self.sender_address
        d['receiver_address'] = self.receiver_address
        d['amount'] = self.amount
        h = SHA.new(json.dumps(d).encode('utf8'))
        signature = hexlify(signer.sign(h)).decode('ascii')
        return signature

    def to_dict(self, for_hash=False):
        d = OrderedDict()
        d['sender_address'] = self.sender_address
        d['receiver_address'] = self.receiver_address
        d['amount'] = self.amount
        if not for_hash:
            d['transaction_id'] = self.transaction_id
            d['transaction_inputs'] = self.transaction_inputs
            d['transaction_outputs'] = self.transaction_outputs
            d['signature'] = self.signature
        return d


class Transaction_Output(object):
    def __init__(self, transaction_id, receiver_address, amount):
        self.transaction_id = transaction_id
        self.receiver_address = receiver_address
        self.amount = amount
        self.id = str(hash((self.transaction_id, self.receiver_address, self.amount)))

    def to_dict(self):
        d = OrderedDict()
        d['id'] = self.id
        d['transaction_id'] = self.transaction_id
        d['receiver_address'] = self.receiver_address
        d['amount'] = self.amount
        return d
