#!/usr/bin/python
import click
import json
import requests
import common

password = common.password

@click.group()
def main():
    """
    Noobcash client.
    """


@main.command()
# το port του node που θα δημιουργησει τη συναλλαγη
@click.argument('ip')
@click.argument('port')
# το port του node που θα λαβει το amount
@click.argument('id')
@click.argument('amount')
def new_transaction(ip, port, id, amount):
    """Make new transaction to a recipient with an amount
       of NBCs that are drawn from your wallet"""
    data = {}
    data['password'] = password
    data['recipient_id'] = id
    data['amount'] = amount
    dict = {'data': json.dumps(data)}

    url = 'http://'+ip+':'+str(port)+'/create_transaction/id'

    response = requests.post(url, data=dict)
    print(response.json()['message'])


@main.command()
@click.argument('ip')
@click.argument('port')
def blockchain(ip, port):
    """View blockchain"""
    url = 'http://'+ip+':'+str(port)+'/view/blockchain'

    response = requests.post(url)
    # print(response.content)

    blockchain = json.loads(response.json()['blockchain'])
    common.prGreen("Blockchain length = ")
    print("%d" % int(blockchain['length']))
    for block in blockchain['blocks']:
        common.prGreen('\nIndex : ')
        print(block['index'])
        common.prGreen('Previous hash = ')
        print(block['previousHash'])
        common.prGreen('Hash = ')
        print(block['Hash'])
        common.prGreen('Nonce = ')
        print(block['nonce'])
        tx_info = 1
        common.prGreen("Transactions:", end="\n")
        for tx in block['listOfTransactions']:
            common.prRed("%d " % tx_info)
            print("%s" % tx['transaction_id'])
            tx_info += 1
    return


@main.command()
# @click.argument('password')
@click.argument('ip')
@click.argument('port')
def view(ip, port):
    """View all transactions in the last validated block"""
    data = {}
    data['password'] = password
    dict = {'data': json.dumps(data)}

    url = 'http://'+ip+':'+str(port)+'/view'

    response = requests.post(url, data=dict)
    block = response.json()['message']

    tx_info = 1
    for tx in block:
        common.prGreen("Transaction %d " % tx_info, end="\n")
        common.prRed("Id =")
        print(" %s" % tx['transaction_id'])
        common.prRed("Amount =")
        print(" %d" % tx['amount'])
        common.prRed("Sender address =")
        print(" %s" % tx['sender_address'])
        common.prRed("Receiver address =")
        print(" %s" % tx['receiver_address'])
        common.prRed("Signature =")
        print(" %s" % tx['signature'])
        common.prRed("Transaction inputs", end="\n")
        tx_input = 1
        for input in tx['transaction_inputs']:
            common.prYellow("UTXO %s id =" % tx_input)
            print(input)
            tx_input += 1
        tx_output = 1
        common.prRed("Transaction outputs", end="\n")
        for output in tx['transaction_outputs']:
            common.prYellow("Transaction Output %s id =" % tx_output)
            print(output['id'])
            common.prYellow("Amount = ")
            print(" %d" % output['amount'])
            common.prYellow("Receiver address =")
            print(" %s" % output['receiver_address'])
            tx_output += 1
        tx_info += 1


@main.command()
# το port του node που θα μας δωσει τις πληροφοριες
@click.argument('ip')
@click.argument('port')
# το id του node που θελουμε το balance
@click.argument('id')
def balance(ip, port, id):
    """Print out wallet balance"""
    data = {}
    data['id'] = id
    data['password'] = password
    dict = {'data': json.dumps(data)}
    
    url = 'http://'+ip+':'+str(port)+'/balance'

    response = requests.post(url, data=dict)
    print(response.json()['message'])


if __name__ == "__main__":
    main()
