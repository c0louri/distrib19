Group assignment for class Distributed Systems at NTUA ECE 2019  
Subject: Blockchain system with Proof of Work, implemented in Python (using Flask)  

Group Members:  
1. Dimitra Koumoutsou
2. Christos Louras
3. Dimitrios Papadopoulos

(Equal contribution)
