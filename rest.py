from flask import Flask, jsonify, request
from flask_cors import CORS
import block
import common
import time
import json
import node
import signal
import threading
import os
from common import prBlack, prCyan, prGreen, prPurple, prYellow, prRed, prLightPurple, prLightGray

# ^ this variable is only important for the backend ofbootstrap node

app = Flask(__name__)
# global varsM
bootstrap_url = "http://"+common.bootstrap_ip + ":" + str(common.bootstrap_port)
bootstrap_ip = common.bootstrap_ip
bootstrap_port = common.bootstrap_port
this_node = node.node()
password = common.password
CORS(app)

# οταν τερματιζουμε με SIGINT το flask
# θα αποθηκευει σε αρχειο το average block time
def signal_handler(signum, frame):
    filename = 'n%d_cap%d_dif%d_%d.out' % (common.max_nodes, common.capacity, common.difficulty, this_node.id)
    with open(filename, 'w') as fp:
        # write total time of execution from 1st transaction
        total_time = this_node.end_time - this_node.start_time
        fp.write('Total time: %f\n' % total_time)
        # initial transactions dont count
        thr = (this_node.chain.length - 2)*common.capacity / total_time
        fp.write('Throughput = %f\n' % thr)
        mean_bl_time = sum(this_node.block_times) / max(len(this_node.block_times), 1)
        fp.write('Blocks mined and added : %d\n' % len(this_node.block_times))
        fp.write('Average block mining time : %f\n' % mean_bl_time)
        
    exit()


signal.signal(signal.SIGINT, signal_handler)

# .........................
# FOR FRONTED


@app.route('/create_transaction/id', methods=['POST'])
def create_transaction_id():
    this_node.lock.acquire()
    data = request.form
    infos = json.loads(data['data'])
    if infos['password'] != password:
        response = {'message': 'Incorrect password!'}
        return jsonify(response), 400
    id = int(infos['recipient_id'])
    amount = int(infos['amount'])
    # check if it is the first tx
    if this_node.start_time is None:
        this_node.start_time = time.time()
    valid, e = this_node.create_transaction(this_node.ring[id]['public_key'], amount)
    if not valid:
        prYellow('[ Transaction ]: ')
        prRed(e, end='\n')
        this_node.lock.release()
        response = {'message': 'Invalid Transaction'}
        return jsonify(response), 400
    else:
        this_node.lock.release()
        response = {'message': 'Transaction Created Successfully'}
        return jsonify(response), 200


@app.route('/create_transaction/recipient_address', methods=['POST'])
def create_transaction_rec_addr():
    infos = request.form
    if infos['password'] != password:
        response = {'message': 'Incorrect password!'}
        return jsonify(response), 400
    recipient_address = infos['recipient']
    amount = infos['amount']
    this_node.lock.acquire()
    # check if it is the first tx
    if this_node.start_time is None:
        this_node.start_time = time.time()
    valid, e = this_node.create_transaction(this_node, recipient_address, amount)
    if not valid:
        prYellow('[ Transaction ]: ')
        prRed(e, end='\n')
        this_node.lock.release()
        response = {'message': 'Invalid Transaction'}
        return jsonify(response), 400
    else:
        this_node.lock.release()
        response = {'message': 'Transaction Created Successfully'}
        return jsonify(response), 200


@app.route('/view', methods=['POST'])
def view():
    this_node.lock.acquire()
    data = request.form
    infos = json.loads(data['data'])
    if infos['password'] != password:
        response = {'message': 'Incorrect password!'}
        return jsonify(response), 400
    last_block = this_node.chain.get_last_block()
    response = {'message': last_block['listOfTransactions']}
    this_node.lock.release()
    return jsonify(response), 200


@app.route('/view/blockchain', methods=['POST'])
def view_blockchain():
    this_node.lock.acquire()
    response = {'blockchain': json.dumps(this_node.chain.to_dict())}
    this_node.lock.release()
    return jsonify(response), 200


@app.route('/balance', methods=['POST'])
def balance():
    data = request.form
    infos = json.loads(data['data'])
    if infos['password'] != password:
        response = {'message': 'Incorrect password!'}
        return jsonify(response), 400
    id = int(infos['id'])
    balance = this_node.get_balance(id)
    response = {'message': balance}
    return jsonify(response), 200

#
# FOR BACKEND
#


@app.route('/new_transaction', methods=['POST'])
def new_transaction():
    this_node.lock.acquire()
    # NODE IS INFORMED OF A NEW TRANSACTION
    # get data of the transactions
    infos = request.form
    tx_dict = json.loads(infos['tx_dict'])
    # validate transaction
    from_id = this_node.get_id(tx_dict['sender_address'])
    to_id = this_node.get_id(tx_dict['receiver_address'])
    prYellow('[ Transaction ]: ')
    print("%d NBC from %d to %d" % (tx_dict['amount'], from_id, to_id))
    is_valid, errno = this_node.validate_transaction(tx_dict)
    if not is_valid:
        prYellow('[ Transaction ]: ')
        prRed("Invalid Transaction! Number "+str(errno), end='\n')
        response = {
            'message': 'Transaction is not valid'
        }
        this_node.lock.release()
        return jsonify(response), 200
    # if valid, add to the current block
    prYellow('[ Transaction ]: ')
    print("Transaction added to pool!")
    this_node.add_transaction_to_pool(tx_dict)
    this_node.lock.release()
    response = {
        'message': 'Transaction added to block!'
    }
    return jsonify(response), 200


@app.route('/new_block', methods=['POST'])
def new_block():
    # NODE IS INFORMED OF A NEW BLOCK
    this_node.lock.acquire()
    infos = request.form
    block_dict = json.loads(infos['block_dict'])
    # η validate_income επιστρεφει:
    # 0 = wrong hash
    # 1 = resolve_conflict
    # 2 = accepted
    prGreen('[ Block ]: ')
    print("Received new block!")
    new_block = block.Block(block_dict['index'], block_dict['timestamp'],
                            block_dict['previousHash'], block_dict['Hash'],
                            block_dict['nonce'], block_dict['listOfTransactions'])
    conflict, message = new_block.validate_income(this_node.chain.get_last_hash(),
                                                  this_node.chain.next_index)
    if conflict == 0:
        prGreen('[ Block ]: ')
        prRed("Reject block. " + message, end='\n')
        response = {'message': 'Block is not valid'}
        this_node.lock.release()
        return jsonify(response), 400
    elif conflict == 1:
        thr = threading.Thread(target=thread_resolve_conflicts, args=(message,))
        thr.start()
        response = {'message': 'Conflict, continue with resolve'}
        return jsonify(response), 200
    else:
        prGreen('[ Block ]: ')
        print('Adding income block!', " ", message)
        success, error = this_node.add_outside_block(block_dict)
        if success:
            this_node.end_time = time.time()
            prGreen('[ Block ]: ')
            print("Block added to existing blockchain!")
            if this_node.mine_process is not None:
                if this_node.mine_process.is_alive():
                    this_node.mine_process.shutdown_flag.set()
            response = {'message': 'Block accepted!'}
        else:
            prGreen('[ Block ]: ')
            prRed("Invalid incoming block! errno %d" % error, end='\n')
            response = {'message': 'Block rejected!'}
        this_node.lock.release()
        return jsonify(response), 200


def thread_resolve_conflicts(message):
    prGreen('[ Block ]: ')
    prCyan("Resolve conflict! " + message, end='\n')
    success = this_node.resolve_conflicts()
    if success:
        this_node.end_time = time.time()
        if this_node.mine_process is not None:
            if this_node.mine_process.is_alive():
                this_node.mine_process.shutdown_flag.set()
    else:
        prGreen('[ Resolve conflict ]: ')
        prRed("Failed!", end='\n')
    this_node.lock.release()
    return


@app.route('/blockchain_size', methods=['POST'])
def blockchain_size():
    this_node.chain_lock.acquire()
    hashes = this_node.chain.get_blocks_hashes()
    prLightPurple('[ Blockchain ]: ')
    print("Serving request about my blockchain size!")
    response = {
        'message': 'Here is my blockchain size',
        'data': (len(hashes), hashes)
    }
    this_node.chain_lock.release()
    return jsonify(response), 200


@app.route('/gimme_blockchain_from_index', methods=['POST'])
def gimme_blockchain_from_index():
    this_node.chain_lock.acquire()
    index = int(request.form['index'])
    prLightPurple('[ Blockchain ]: ')
    print("Sending blocks from index %d!" % index)
    blocks = this_node.chain.to_dict()['blocks'][index:]
    # give also tx_pool
    # give also ring with the updated utxos for each node
    response = {
        'message': 'lave ta blocks mou',
        'blocks': json.dumps(blocks),
        'ring': json.dumps(this_node.ring)
    }
    this_node.chain_lock.release()
    return jsonify(response), 200


@app.route('/update/nodes_info', methods=['POST'])
def update_nodes_info():
    this_node.lock.acquire()
    # get info from request from
    # update ring
    if this_node.bootstrap:
        # Bootstrap doesnt need updates for the ring
        response = {'message': 'Wrong requests!'}
        this_node.lock.release()
        return jsonify(response), 400
    else:
        new_node = json.loads(request.form['node_dict'])
        this_node.ring.append(new_node)
        prCyan('[ Ring ]: ')
        print("New node in the Network!")
        response = {'message': 'mou \'rthan ta nea'}
        this_node.lock.release()
        return jsonify(response), 200


# requests to /register_node can be served ONLY by bootstrap
@app.route('/register_node', methods=['POST'])
def register_node():
    this_node.lock.acquire()
    # to be used only by bootstrap
    if not this_node.bootstrap:
        response = {
            'message': 'I am not BOOTSTRAP'
        }
        this_node.lock.release()
        return jsonify(response), 400
    new_node = {
        'id': this_node.next_id,
        'ip': request.form['ip'],
        'port': request.form['port'],
        'public_key': request.form['public_key'],
        'utxos': {}
    }
    # register node into the ring and broadcast this to the known nodes of
    # the network
    success = this_node.register_node_to_ring(new_node)
    if success:
        response = {
            'message': 'Successful registration',
            'id': new_node['id'],
            'ring': this_node.ring,
            'blockchain': this_node.chain.to_dict()['blocks']
        }
    else:
        response = {'message': 'ERROR'}
    this_node.lock.release()
    return jsonify(response), 200


def wr_connect(url, ip, port):
    this_node.lock.acquire()
    success = this_node.connect_to_network(url, ip=ip, port=port)
    this_node.lock.release()
    if not success:
        prCyan('[ Ring ]: ')
        print("Cant connect!")
        os._exit(0)
    return


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-ip', '--ip_addr', default='0.0.0.0',
                        type=str, help='IP to listen on')
    parser.add_argument('-p', '--port', default=10000,
                        type=int, help='port to listen on')
    parser.add_argument('-b', '--is_bootstrap', default=0,
                        type=int, help='>0 -> Bootstrap')
    parser.add_argument('-d', '--debug', default=0,
                        type=int, help='>0 -> Debug')
    args = parser.parse_args()
    port = args.port
    ip_addr = args.ip_addr
    debug = args.debug
    is_bootstrap = bool(args.is_bootstrap)
    if is_bootstrap:
        bootstrap_ip = ip_addr
        bootstrap_port = port
        this_node.become_bootstrap(ip=bootstrap_ip, port=bootstrap_port)
    else:
        prCyan('[ Ring ]: ')
        print("Trying connecting to network with bootstrap on", bootstrap_ip, ":", bootstrap_port)
        thread1 = threading.Thread(target=wr_connect,
                                   args=(bootstrap_url+'/register_node', ip_addr, port))
        thread1.start()
    app.run(host=ip_addr, port=port, debug=bool(debug), use_reloader=False)
