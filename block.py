import hashlib
from Crypto.Hash import SHA256
from collections import OrderedDict
import common
import json

diff = common.difficulty


class Block(object):
    def __init__(self, index, timestamp, previousHash,
                 Hash=None, nonce=None, listOfTransactions=None):
        self.index = index
        self.previousHash = previousHash
        self.timestamp = timestamp
        self.Hash = Hash
        self.nonce = nonce
        self.listOfTransactions = listOfTransactions

    # για καθε μπλοκ οταν κανουμε validate το chain
    def validate(self, previousHash, difficulty=diff):
        goal = '0' * difficulty
        data = self.timestamp + self.previousHash
        for tx in self.listOfTransactions:
            data += json.dumps(tx)
        hash = calc_hash(data, self.nonce)
        same_hash = hash == self.Hash
        goal_achieved = hash_hex_to_bin(hash).startswith(goal)
        samePrevHash = previousHash == self.previousHash
        return (same_hash and goal_achieved and samePrevHash) 

    # return values της validate_income
    # 0 -> απορριψε το καινουργιο πακετο
    # 1 -> καλεσε τη resolve_confict
    # 2 -> δεξου το καινουργιο πακετο
    def validate_income(self, previousHash, our_index, difficulty=diff):
        common.prGreen('[ Block ]: ')
        print("Validate income block")
        goal = '0' * difficulty
        data = json.dumps(self.timestamp) + self.previousHash
        for tx in self.listOfTransactions:
            data += json.dumps(tx)
        hash = calc_hash(data, self.nonce)
        same_hash = hash == self.Hash
        goal_achieved = hash_hex_to_bin(hash).startswith(goal)
        valid = same_hash and goal_achieved
        # αν ειναι λαθος το hash απορριπτεται
        # ή το μηκος του blockchain μας ειναι μικροτερο απο του αλλου
        if not valid or (self.index < our_index):
            if not valid:
                return 0, "not valid"
            else:
                return 0, 'older index'
            return 0
        # αν το blockchain του αλλου ειναι μεγαλυτερο απο το δικο μας
        # καλεσε τη resolve_confict
        if self.index > our_index:
            return 1, 'longer chain'
        # διαφορετικα, αν ειμαστε ακριβως στο ιδιο σημειο του Blockchain
        # δεξου τη οπως εισαι
        elif previousHash == self.previousHash:
            return 2, 'can be added to our blockchain'
        else:
            # διαφορετικα αν εχουμε διαφορετικα Hashes
            # καλεσε παλι τη resolve_conflict
            return 1, ' dif prev hash'

    def to_dict(self):
        d = OrderedDict()
        d['index'] = self.index
        d['previousHash'] = self.previousHash
        d['timestamp'] = self.timestamp
        d['Hash'] = self.Hash
        d['nonce'] = self.nonce
        d['listOfTransactions'] = self.listOfTransactions
        return d


class Blockchain(object):
    def __init__(self, blocks=[]):
        self.listOfBlocks = blocks  # list of Block objects
        self.next_index = self.length = len(blocks)
        # self.length = len(blocks)

    def to_dict(self):
        d = OrderedDict()
        d['next_index'] = self.next_index
        d['length'] = self.length
        d['blocks'] = [block.to_dict() for block in self.listOfBlocks]
        return d

    def get_last_block(self):
        return self.listOfBlocks[-1].to_dict()

    def get_length(self):
        return self.length

    def get_last_hash(self):
        return self.listOfBlocks[-1].Hash

    # validate blockchain
    def validate(self):
        for i in range(1, self.length):
            cur = self.listOfBlocks[i]
            prev = self.listOfBlocks[i-1]
            # for current check if previousHash is valid
            if cur.previousHash != prev.Hash:
                return False, 0, i
            # for current check if Hash is valid
            valid = cur.validate(prev.Hash)
            if not valid:
                return False, 1, i
        return True, None, None

    def get_blocks_hashes(self):
        hashes = [(block.index, block.Hash) for block in self.listOfBlocks]
        return hashes

    def add_block(self, block):
        self.listOfBlocks.append(block)
        self.next_index += 1
        self.length += 1


def create_blockchain(block_list):
    blocks = []
    for block_dict in block_list:
        new_block = Block(block_dict['index'], block_dict['timestamp'],
                          block_dict['previousHash'], block_dict['Hash'],
                          block_dict['nonce'], block_dict['listOfTransactions'])
        blocks.append(new_block)
    return Blockchain(blocks=blocks)


def calc_hash(data, nonce):
    s = data + str(nonce)
    digest = hashlib.sha256(s.encode('ascii')).hexdigest()
    return digest

def hash_hex_to_bin(digest):
    return format(int(digest, 16), '0256b')
