import block
import common
import transaction
import requests
import threading
import json
import time
from threading import Thread, Event, Lock
from copy import deepcopy
import datetime
from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from collections import OrderedDict
from binascii import unhexlify, hexlify
from operator import itemgetter
from common import prBlack, prCyan, prGreen, prPurple, prYellow, prRed, prLightPurple, prLightGray


MAX_NODES = common.max_nodes
CAPACITY = common.capacity
MINING_DIFFICULTY = common.difficulty

# # # # # #
# ring is a list of dict_nodes
# doct_nodes is a dict of 'id', 'ip', 'port', 'public_key' and 'utxos'
# utxos is a dict of dict_utxo (KEY: utxo_id, DATA: dict_utxo)
# dict_utxo is a dict of Transaction_Output
# # # # # #

# # # # # #
# wallet is an object (1 for every node) with:
# private Key
# public key
# # # # # #

# # # # # #
# tx_pool is an Ordered dict of dict_transaction (KEY: transaction_id, DATA: dict_transaction)
# dict_transaction is a dict of Transaction
# # # # # #


class node(object):
    def __init__(self):
        # by default, every node is NOT bootstrap
        self.lock = Lock()
        self.chain_lock = Lock()
        self.alive_nodes = None
        self.mine_process = None
        self.bootstrap = False
        self.chain = None
        self.next_id = -1
        self.id = -1
        # next_id -> useful only if node is bootstrap_port
        # keeps the next available id which ca be given to a new node
        self.wallet = Wallet()
        self.ring = []
        # dict απο transactions μεγιστου μεγεθους CAPACITY για το νεο block
        self.tx_pool = OrderedDict()
        self.block_times = []
        self.start_time = None
        self.end_time = None
        return

    # συναρτηση που εκτελειται απο τον bootstrap για την αρχικοποιηση του δικτυου
    def become_bootstrap(self, ip, port):
        self.bootstrap = True
        self.alive_nodes = 1
        self.id = 0
        self.next_id = 1
        # δημιουργια του bootstrap-στοιχειου του ring
        new_node = {}
        new_node['id'] = 0
        new_node['ip'] = ip
        new_node['port'] = str(port)
        new_node['public_key'] = self.wallet.public_key
        new_node['utxos'] = {}
        self.ring.append(new_node)
        # δημιουργια του blockchain με το genesis block
        self.chain = block.Blockchain([self.genesis_block()])
        return

    # Συναρτηση που δημιουργει το genesis block
    def genesis_block(self):
        # create genesis transaction
        amount = MAX_NODES * 100
        gen_tx = transaction.Transaction(sender_wallet=None,
                                         receiver_address=self.ring[0]['public_key'],
                                         amount=amount, node=self
                                         ).to_dict()
        timestamp = str(datetime.datetime.now())
        prevHash = "1"
        data = timestamp + prevHash + json.dumps(gen_tx)
        hash = block.calc_hash(data=data, nonce=0)
        bl = block.Block(0, timestamp, prevHash, hash, 0, [gen_tx])
        return bl

    # Αιτημα απο καθε νεο κομβο για εγγραφη στο δικτυο
    # αντιστοιχα ενημερωνεται το id, το ring και το blockchain του
    def connect_to_network(self, bs_url, ip, port):
        # bs_url -> url of bootstrap server for registering new nodes
        if self.bootstrap:
            print("Error! This node is BOOTSTRAP!")
            exit()
        # send request to bootstrap node to be regitered in the network
        node_dict = {}
        node_dict['ip'] = ip
        node_dict['port'] = port
        node_dict['public_key'] = self.wallet.public_key
        # if no 0K response exit
        try:
            response = requests.post(bs_url, data=node_dict)
        except requests.exceptions.RequestException as e:
            print(e)
            return False
        else:
            # registration is Successful
            prGreen(response.json()['message'], end='\n')
            if response.json()['message'] == 'ERROR':
                return False
            else:
                self.id = response.json()['id']
                self.ring = response.json()['ring']
                self.chain = block.create_blockchain(response.json()['blockchain'])
                return True

    # συναρτηση που καλειται απο το bootstrap για την εξυπηρετηση του αιτηματος
    # εγγραφης του νεου κομβου
    def register_node_to_ring(self, node_dict):
        '''
        add this node to the ring, only the bootstrap node can add
        a node to the ring after checking his wallet and ip:port address
        '''
        id = self.check_if_already_known(node_dict['ip'], node_dict['port'])
        if id >= 0:
            # υπαρχει αλλος κομβος με την ιδια ip και port
            return False
        self.ring.append(node_dict)
        self.next_id += 1
        data = {'node_dict': json.dumps(node_dict)}
        # infrom the other nodes about the new node
        for i in range(1, len(self.ring)-1):
            node = self.ring[i]
            url = 'http://'+node['ip']+':'+str(node['port'])+'/update/nodes_info'
            response = requests.post(url, data=data)
            if response.status_code != 200:
                prRed("Failed updating nodes info to " + url, end='\n')
                exit()
        self.alive_nodes += 1
        # send 100 NBC to node
        if self.alive_nodes <= MAX_NODES:
            t = threading.Thread(target=self.thr_send_first_tx, args=(node_dict['public_key'], 100))
            t.start()
        return True

    # Συναρτηση που εξασφαλιζει πως ο κομβος που αιτηθηκε
    # εγγραφη στο δικτυο δεν υπαρχει ηδη
    def check_if_already_known(self, ip, port):
        for node in self.ring:
            if (node['ip'] == ip) and (str(node['port']) == str(port)):
                return node['id']
        return -1

    # Η get_id επιστρεφει το id ενος node με δεδομενο το public_key του
    def get_id(self, public_key=None):
        if public_key is None:
            return None
        for node in self.ring:
            if node['public_key'] == public_key:
                return node['id']
        return None

    # η get_balance επιστρεφει το balance με δεδομενο το id του node
    # υπολογιζεται διατρεχοντας τα utxos του ring
    def get_balance(self, id):
        balance = 0
        for node_dict in self.ring:
            if node_dict['id'] == id:
                for utxo in node_dict['utxos'].values():
                    balance += utxo['amount']
        return balance

    # καλειται καθε φορα για να δημιουργηθει ενα transaction
    def create_transaction(self, receiver_address, amount):
        prYellow('[ Transaction ]: ')
        print("%d NBC from %d to %d" % (amount, self.id, self.get_id(receiver_address)))
        # makes transaction - updates ring
        try:
            tx = transaction.Transaction(self.wallet, receiver_address, amount, self)
        except ValueError as e:
            return False, e
        else:
            # προσθετει το transaction στο προσωπικο pool του node που το δημιουργησε
            prYellow('[ Transaction ]: ')
            print("Transaction added to pool!")
            self.add_transaction_to_pool(tx.to_dict())
            # δημοσιευει το transaction σε ολους τους κομβους
            self.broadcast_transaction(tx.to_dict())
        return True, None

    def broadcast_transaction(self, tx_dict):
        prGreen('[ Transaction ]:')
        print('Broadcast transaction!')
        data = {'tx_dict': json.dumps(tx_dict)}
        thread_list = []
        for i in range(0, len(self.ring)):
            if self.id != i:
                node = self.ring[i]
                url = 'http://'+node['ip']+':'+str(node['port'])+'/new_transaction'
                t = threading.Thread(target=self.thr_send_data, args=(url, data))
                t.start()
                thread_list.append(t)
        return

    def validate_transaction(self, tx_dict):
        # αναζητηση του sender μεσα στο ring
        from_id = self.get_id(tx_dict['sender_address'])
        node_dict = self.ring[from_id]
        # για τσεκαρισμα της υπογραφης
        public_key = RSA.importKey(unhexlify(node_dict['public_key']))
        verifier = PKCS1_v1_5.new(public_key)
        # δημιουργια του dictionary τέτοιο ώστε να είναι ίδιο με αυτό
        # από το οποίο προέκυψε το signature
        d = OrderedDict()
        d['sender_address'] = tx_dict['sender_address']
        d['receiver_address'] = tx_dict['receiver_address']
        d['amount'] = tx_dict['amount']

        # Ελεγχος της υπογραφής
        h = SHA.new(json.dumps(d).encode('utf8'))
        if not verifier.verify(h, unhexlify(tx_dict['signature'])):
            return False, 0

        # ελεγχος των inputs
        utxos = node_dict['utxos']  # περιεχει τα utxos στο wallet του sender
        inputs = tx_dict['transaction_inputs']  # περιεχει τα ids των utxos στην συναλλαγή
        inputs_amount = 0  # κρατάμε το έως τώρα πσοό που έχουμε βρει
        'j -> id of an utxo'
        utxos_to_be_removed = []
        for j in inputs:
            if j not in utxos:
                # το id δεν υπάρχει στα utxo του wallet, οπότε η
                # συναλλαγή θεωρείται άκυρη
                return False, 1
            else:
                utxos_to_be_removed.append(j)
                inputs_amount += node_dict['utxos'][j]['amount']

        # ελεγχος amount - inputs
        if (inputs_amount < tx_dict['amount']):
            # δεν υπάρχουν αρκετά διαθεσιμα utxos για την συναλλαγή
            # οπότε την θεωρούμε άκυρη
            return False, 2
        # ελεγχος inputs - outputs
        # το άθροισμα των ποσών στα transaction inputs πρεπει να είναι
        # ίσο με το άθροισμα των ποσών στα transaction outputs
        outputs_amount = 0
        for output in tx_dict['transaction_outputs']:
            outputs_amount += output['amount']
        if inputs_amount != outputs_amount:
            return False, 3

        # διαγραφη των utxos που χρησιμοποιηθηκαν
        for utxo_id in utxos_to_be_removed:
            del node_dict['utxos'][utxo_id]
        # προσθηκη των νεων utxos που προκυπτουν απο τη συναλλαγη
        for output in tx_dict['transaction_outputs']:
            receiver_id = self.get_id(output['receiver_address'])
            key = output['id']
            self.ring[receiver_id]['utxos'].update({key: output})
        return True, None

    # καλειται μετα την add_transaction_to_pool οταν len(pool) == CAPACITY
    def create_new_block(self):
        timestamp = str(datetime.datetime.now())
        prevHash = self.chain.get_last_hash()
        # take capacity TRANSACTIONS
        tx_list = []
        for i in range(0, CAPACITY):
            tx_list.append(list(self.tx_pool.values())[i])
        # να θυμηθουμε να αφαιρεσουμε τα txs απο το pool
        # σε περιπτωση επιτυχημενου mining
        bl = block.Block(self.chain.next_index, timestamp, prevHash,
                         listOfTransactions=tx_list)
        self.mine_process = Mine(node=self, block=bl, difficulty=MINING_DIFFICULTY)
        self.mine_process.start()
        return

    # καλειται μετα αν η validate_transaction επιστρεφει True
    def add_transaction_to_pool(self, tx_dict):
        self.tx_pool.update({tx_dict['transaction_id']: tx_dict})
        # if enough transaction, create block and start mining
        if len(self.tx_pool) >= CAPACITY:
            if self.mine_process is None:
                self.create_new_block()
            elif not self.mine_process.is_alive():
                self.create_new_block()

    def broadcast_block(self, block_dict):
        prGreen('[ Block ]: ')
        print("Broadcast mined block!")
        data = {'block_dict': json.dumps(block_dict)}
        thread_list = []
        for i in range(0, len(self.ring)):
            if self.id != i:
                node = self.ring[i]
                url = 'http://'+node['ip']+':'+str(node['port'])+'/new_block'
                t = threading.Thread(target=self.thr_send_data, args=(url, data))
                t.start()
                thread_list.append(t)
        return

    # διαγραφει τα transaction που ανηκουν στο μπλοκ που μολις εγινε mine
    # καλειται μονο απο τη συναρτηση run της κλασης Mine
    def clear_pool_from_used_transactions(self, used_transactions):
        prGreen('[ Block ]: ')
        print("Clear pool from used tx!")
        for used_transaction in used_transactions:
            if used_transaction['transaction_id'] in self.tx_pool:
                del self.tx_pool[used_transaction['transaction_id']]

    # εισαγει στο blockchain μας ενα καινουργιο μπλοκ το οποιο εγινε
    # mined απο αλλον κομβο
    def add_outside_block(self, block_dict):
        index = self.chain.next_index
        new_block = [block_dict]
        old_blocks = self.chain.listOfBlocks[0: index]
        old_blocks_dict = [old_block.to_dict() for old_block in old_blocks]
        new_chain = block.create_blockchain(old_blocks_dict+new_block)
        valid, error = self.update_utxos_and_tx_pool(index, new_chain)
        if not valid:
            return False, error
        else:
            self.chain = new_chain
            return True, None

    # ρωτα καθε αλλον κομβο για το μεγεθος του blockchain τους
    # κι επιλεγει το μεγαλυτερο απο αυτα (με δεδομενο οτι ειναι μεγαλυτερο απο το δικο του)
    # και το κανει validate
    # Αν δεν ειναι εγκυρο, τοτε ελεγχει το αμεσως επομενο καλυτερο
    def resolve_conflicts(self):
        my_chain = self.chain.get_blocks_hashes()
        # 1st -> node_id, 2nd -> length, 3rd -> indexes and hashes
        infos = [(self.id, len(my_chain), my_chain)]
        # κανουμε το αιτημα σε ολους τους κομβους
        for i in range(0, len(self.ring)):
            if (self.id != i):
                node = self.ring[i]
                ip = node['ip']
                port = node['port']
                url = 'http://'+ip+':'+str(port)+'/blockchain_size'
                try:
                    response = requests.post(url)
                except requests.exceptions.RequestException as e:
                    print(e)
                else:
                    # μολις λαβαμε ενα response
                    length = response.json()['data'][0]
                    # αν το blockchain που πηραμε ειναι μικροτερο-ισο
                    # του δικου μας το αγνοουμε
                    if length <= len(my_chain):
                        continue
                    # διαφορετικα το προσθετουμε στη λιστα προς ελεγχο
                    indexes_hashes = response.json()['data'][1]
                    infos.append((i, length, indexes_hashes))
        # ταξινομησε τη λιστα best με βαση το μηκος του blockchain
        sorted(infos, key=itemgetter(1), reverse=True)
        # τσεκαρουμε ενα ενα τα blockchain απο το μεγαλυτερο στο μικροτερο
        # το πρωτο εγκυρο που θα βρουμε το κραταμε
        for info in infos:
            '''
            info[0] -> id
            info[1] -> length of chain
            info[2] -> output of get_blocks_hashes
            '''
            his_her_chain = info[2]
            index = 0
            # ελεγχουμε ποσα απο τα blocks απο index 0 ειναι κοινα με το δικο μας
            for i in range(0, len(my_chain)):
                if my_chain[i][1] != his_her_chain[i][1]:  # CHANGED
                    index = i
                    break
            # το genesis block ειναι κοινο για ολους
            if index == 0:
                continue
            # ζηταμε απο το node με το μεγαλυτερο μηκος,
            # το blockchain του (οσο απο αυτο ειναι διαφορετικα απο το δικο μας)
            node = self.ring[info[0]]
            ip = node['ip']
            port = node['port']
            url = 'http://'+ip+':'+str(port)+'/gimme_blockchain_from_index'
            try:
                data = {'index': index}
                response = requests.post(url, data=data)
            except requests.exceptions.RequestException as e:
                print(e)
            else:
                # ενημερωση blockchain
                # τα blocks που θα προστεθουν
                new_blocks = json.loads(response.json()['blocks'])
                # τα blocks μας που θα κρατησουμε
                old_blocks = self.chain.listOfBlocks[0: index]
                old_blocks_dict = [old_block.to_dict() for old_block in old_blocks]
                # ελεγχος ολοκληρου του νεου blockchain
                new_chain = block.create_blockchain(old_blocks_dict + new_blocks)
                valid, errno, bl_ind = new_chain.validate()
                if not valid:
                    prGreen('[ Resolve Conflict ]: ')
                    print("New blockchain from has wrong hashes. Errno=%d, ind=%d" % (errno, bl_ind))
                valid, error = self.update_utxos_and_tx_pool(index, new_chain)
                if valid:
                    self.chain_lock.acquire()
                    self.chain = new_chain
                    self.chain_lock.release()
                    prGreen('[ Resolve Conflict ]: ')
                    print("New blockchain from node with id = %d" % (info[0]))
                    return True
        return False

    # τρεχει απο το bootstrap για την αποστολη των πρωτων transactions
    def thr_send_first_tx(self, address, amount):
        while True:
            # κωδικας για την αποστολη μαζεμενων των αρχικων transactions ή οχι
            '''
            if self.alive_nodes < MAX_NODES:
                continue
            '''
            time.sleep(1)
            self.lock.acquire()
            self.create_transaction(address, amount)
            self.lock.release()
            break
        return

    # η συναρτηση που θα τρεχει καθε thread υπευθυνο για την αποστολη δεδομενων
    def thr_send_data(self, url, data):
        try:
            requests.post(url, data=data)
        except requests.exceptions.RequestException as e:
            print(e)
        return

    # καλειται ειτε απο την resolve_conflicts, ειτε απο την add_outside_block
    # για την ενημερωση των utxos στο ring και του pool
    # index -> το σημειο που αλλαζουν τα blockchain για πρωτη φορα
    # new_chain -> τα νεα blocks που θα προστεθουν
    def update_utxos_and_tx_pool(self, index, new_chain):
        # backup utxos of ring
        utxos = []
        for node in self.ring:
            utxos.append(deepcopy(node['utxos']))
            node['utxos'] = {}
        # clear utxos of rings
        # add new utxos  for common blocks
        for bl in self.chain.listOfBlocks[:index]:
            for tx in bl.listOfTransactions:
                sender_id = self.get_id(tx['sender_address'])
                for tx_input in tx['transaction_inputs']:
                    v = self.ring[sender_id]['utxos'].pop(tx_input, None)
                    if v is None:
                        # restore self.ring utxos dicts
                        for i in range(0, len(self.ring)):
                            self.ring[i]['utxos'] = utxos[i]
                        return False, 0

                for tx_output in tx['transaction_outputs']:
                    receiver_id = self.get_id(tx_output['receiver_address'])
                    key = tx_output['id']
                    self.ring[receiver_id]['utxos'].update({key: tx_output})
        # προσθεσε στην αρχη του tx_pool τις συναλλαγες που υπαρχουν στα blocks που θα πεταξουμε
        existing_tx_pool = deepcopy(self.tx_pool)
        new_tx_pool = OrderedDict()
        for bl in self.chain.listOfBlocks[index:]:
            for tx in bl.listOfTransactions:
                new_tx_pool.update({tx['transaction_id']: tx})
        new_tx_pool.update(existing_tx_pool)
        # τσεκαρουμε τα νεα blocks και διαγραφουμε απο το νεο tx pool τις συναλλαγες
        # που υπαρχουν σε αυτα, παραλληλα κανουμε validate καθε νεο utxo βασει αυτού
        for bl in new_chain.listOfBlocks[index:]:
            for tx in bl.listOfTransactions:
                valid, message = self.validate_transaction(tx)
                if not valid:
                    # restore ring
                    for i in range(0, len(self.ring)):
                        self.ring[i]['utxos'] = utxos[i]
                    return False, 1
                # διαγραφη απο το tx pool αν υπαρχει
                if tx['transaction_id'] in new_tx_pool:
                    new_tx_pool.pop(tx['transaction_id'], None)
        # validate των στοιχειων του pool και διαγραφη των αντιστοιχων
        self.tx_pool = OrderedDict()
        for tx in new_tx_pool.values():
            is_valid, ernno = self.validate_transaction(tx)
            if is_valid:
                self.add_transaction_to_pool(tx)
        return True, None


# κλαση υπευθυνη για το Mining
# αυτη η κλαση τρεχει απο διαφορετικο Thread, ωστε να γινεται παραλληλα με
# τις υπολοιπες εργασιες του κομβου
class Mine(Thread):
    def __init__(self, node, difficulty=1, block=None):
        Thread.__init__(self)
        self.block = block
        self.goal = '0' * difficulty
        self.shutdown_flag = Event()
        # σημα τερματισμου ερχεται οταν λαμβανουμε block απο αλλο node
        # που ειναι valid κι εχει index μεγαλυτερο ή του next_index
        # του δικου μας chain, το οποιο και εισαγεται στο Blockchain μας
        # ετσι και η συνεχεια της αναζητησης nonce ειναι περιττη
        self.node = node
        self.nonce = None
        self.digest = None
        self.start_time = None

    def run(self):
        self.start_time = time.time()
        prGreen('[ Block ]: ')
        print("Starting mining!!!")
        # create hash message
        data = json.dumps(self.block.timestamp) + self.block.previousHash
        for tx in self.block.listOfTransactions:
            data += json.dumps(tx)
        nonce = 0
        # οσο δεν ερχεται σημα τερματισμου
        while not self.shutdown_flag.is_set():
            digest = block.calc_hash(data, nonce)
            goal_achieved = block.hash_hex_to_bin(digest).startswith(self.goal)
            if goal_achieved:
                self.nonce, self.digest = nonce, digest
                # add mined block in Blockchain
                self.block.nonce = nonce
                self.block.Hash = digest
                self.node.lock.acquire()
                if self.shutdown_flag.is_set():
                    prGreen('[ Block ]: ')
                    prRed("Aborting mining!", end='\n')
                    if len(self.node.tx_pool) >= CAPACITY:
                        self.node.create_new_block()
                    self.node.lock.release()
                    return
                prGreen('[ Block ]: ')
                print("Finished Mining!!!")
                prGreen('[ Block ]: ')
                print("Adding mined block!!!")
                self.node.chain.add_block(self.block)
                # add time of mining
                self.node.block_times.append(time.time() - self.start_time)
                # update end time
                self.node.end_time = time.time()
                self.node.broadcast_block(self.block.to_dict())
                self.node.clear_pool_from_used_transactions(self.block.listOfTransactions)
                prGreen('[ Block ]: ')
                print("Finished mining and adding!!!")
                
                # ελεγξε αν υπαρχουν αρκετες συναλλαγες για επομενο mining
                if len(self.node.tx_pool) >= CAPACITY:
                    self.node.create_new_block()
                self.node.lock.release()
                return
            else:
                nonce += 1

        prGreen('[ Block ]: ')
        prRed("Stopped Mining!", end='\n')
        self.node.lock.acquire()
        if len(self.node.tx_pool) >= CAPACITY:
            self.node.create_new_block()
        self.node.lock.release()
        return

# κλαση wallet που περιεχει τα public και private key του κομβου μας
class Wallet(object):
    def __init__(self):
        key = RSA.generate(1024)
        self.private_key = key
        self.public_key = hexlify(key.publickey().export_key(format='DER')).decode('ascii')
